const loadButton = document.getElementById('btn');
const listBody = document.getElementById('list');


loadButton.addEventListener('click', ()=>{
    fetch('/js/countries.json')
    .then((resp)=>{
        return resp.json();
    }).then((data)=>{
        listBody.innerHTML = '';
        
        data.forEach(e => {
            listBody.innerHTML += '<li>' + e.name + '</li>' ;
        });
    })
})
